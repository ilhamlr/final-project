<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable =[
        'id','full_name', 'salary', 'workplace', 'activity_status','file_photo', 'file_cv', 'phone_number', 'address', 'framework', 'language_program', 'level'
    ];
}
