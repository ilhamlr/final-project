<?php

namespace App\Http\Controllers;

use App\Vacancy;
use App\User;
use App\Profile;
use Illuminate\Http\Request;
use Alert;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vacancies = Vacancy::all();
        
        // $user = User::find(1);
        // dd($vacancies);
        return view('user/index', compact(['vacancies','user']));
        
    }

    public function home()
    {
        // $user = User::find($id);
       
    }
    public function profile($id)
    {
        $user = User::find($id);
        // $full_name = $user->profile->full_name;
        // return view('user/profile/show', compact(['user','full_name']));
        // dd($user->profile->full_name);
         return view('user/profile/show', compact('user'));
    }
    public function edit($id)
    {
        $user = User::find($id);
        // dd($user->profile->salary);
        return view('user/profile/edit', compact('user'));
    }



    public function halo()
    {
        return view('halo');
    }

    public function template()
    {
        //menggunakan sweet alert
        alert::success('success', 'success');

        return view('cobatemplate');
    }
}
