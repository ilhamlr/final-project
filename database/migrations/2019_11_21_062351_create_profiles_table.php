<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name');
            $table->string('salary');
            $table->string('activity_status');
            $table->string('workplace');
            $table->enum('project_status', ['available', ' not_available']);
            $table->string('file_photo');
            $table->string('file_cv');
            $table->string('phone_number');
            $table->string('address');
            $table->string('language_program');
            $table->string('framework');
            $table->string('level');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
