<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
|gi contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// Route::get('/users','UserController@index');
// Route::get('/users/profile/{id}','ProfileController@show');

Auth::routes();

// menggunakan middleware(admin, auth), route grup dan route prefix
//middleware auth => memastiakan user ter authentikasi
//middleware Admin => memastikan jika users role = 1 maka users tsb admin
//middleware User => memastik   an halaman hanya bisa diakses oleh user
//jika tidak maka redirect->back();

Route::group(['prefix' => 'admin', 'middleware' => ['admin', 'auth']], function () {
    Route::get('/halo', 'UserController@adminHome'); //localhost:8000/admin/halo
    Route::resource('skills', 'SkillController');
    Route::get('/logout', 'Auth\LoginController@logout'); //=> logout admin
});
Route::group(['prefix' => 'user', 'middleware' => ['user', 'auth']], function () {
    Route::get('/template', 'HomeController@template'); //localhost:8000/admin/halo
    Route::get('/index', 'HomeController@index');
    Route::get('/profile/{id}', 'HomeController@profile')->name('profile.show');
    Route::get('/profile/setting/{id}', 'HomeController@edit');
    Route::get('/profile/upload/{id}', 'ProfileController@upload');
    Route::patch('/profile/update/{id}','ProfileController@update');
    // Route::get('/home', 'UserController@userHome')->name('user.home');
    Route::get('/logout', 'Auth\LoginController@logout'); //=> logout users

   });
Route::get('/home', 'UserController@userHome')->name('user.home');
    

// Route::resource('profiles', 'ProfileController');
// Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/template', 'HomeController@template');
