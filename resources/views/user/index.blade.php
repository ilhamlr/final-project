@extends('user.layouts.app')

@section('content')
<div class="container">
    
    <section class="content-header">
        <section class="content">

            <!-- Default box -->
            @foreach ($vacancies as $vacancy)
            <div class="card card-solid">
                <div class="card-body pb-0">
                    <div class="row d-flex align-items-stretch">
                        <div class="col-12">
                            <div class="card bg-light">
                                <!-- <div class="card-header text-muted border-bottom-0">
                                    Digital Strategist
                                </div> -->
                                <div class="card-body pt-0">
                                    <div class="row">
                                        <div class="col-7">
                                            <h2 class="lead"><b>Programmer {{ $vacancy->skill_req}}</b></h2>
                                            <hr>
                                            <li class="text-muted text-sm"><b>Skill : </b> {{ $vacancy->skill_req}}</li>
                                            <li class="text-muted text-sm"><b>Salary : </b> {{ $vacancy->salary}}</li>
                                            <li class="text-muted text-sm"><b>Type : </b> {{ $vacancy->type}}</li>
                                            <li class="text-muted text-sm"><b>Duration : </b> {{ $vacancy->duration}}</li>
                                            <ul class="ml-4 mb-0 fa-ul text-muted">
                                                <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> {{ $vacancy->location}}</li>
                                            </ul>
                                        </div>
                                        <div class="col-5 text-center">
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="text-right">
                                        <a href="https://api.whatsapp.com/send?phone=6289697364398&text=saya%20name%20tertarik%20dengan%20pekerjaan%20id_vacancy" class="btn btn-sm bg-teal">
                                            <i class="fas fa-comments"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
</div>

</div>
<!-- /.card -->

</section>


</div>
</section>
</div>

<!-- /.content -->
@endsection