@extends('user.layouts.app')

@section('content')
<div class="container">
    <section class="content-header">
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        <!-- Profile Image -->
                        <div class="card card-primary card-outline">
                            <div class="card-body box-profile">
                                <div class="text-center">
                                    <img class="profile-user-img img-fluid img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">
                                </div>
                                <!-- '', 'salary', 'workplace', 'activity_status','file_photo', 'file_cv', 'phone_number', 'address',  framework, level -->
                                <h3 class="profile-username text-center">{{ $user->profile->full_name }}</h3>

                                <div class="form-group">
                                    <label for="exampleInputFile">Upload Image</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="exampleInputFile">
                                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                        </div>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">Upload</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputFile">Upload CV</label>
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="exampleInputFile">
                                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                        </div>
                                        <div class="input-group-append">
                                            <span class="input-group-text" id="">Upload</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                        <!-- About Me Box -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">About Me</h3>
                            </div>
                            <form action="/user/profile/update/{{ $user->profile->id }}" method="post"> 
                                {{ csrf_field() }}
                                {{ method_field('PATCH') }}
                                <div class="card-body">

                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Phone</label>
                                        <input type="text" class="form-control" name='phone_number' value='{{ $user->profile->phone_number }}'>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Address</label>
                                        <input type="text" class="form-control" name='address' value='{{ $user->profile->address}}'>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Job</label>
                                        <input type="text" class="form-control" name='activity_status' value='{{ $user->profile->activity_status}}'>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Workplace</label>
                                        <input type="text" class="form-control" name='workplace' value='{{ $user->profile->workplace}}'>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Salary</label>
                                        <input type="text" class="form-control" name='salary' value='{{ $user->profile->salary}}'>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Program Languages</label>
                                        <input type="text" class="form-control" name='language_program' value='{{ $user->profile->language_program }}'>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Frameworks</label>
                                        <input type="text" class="form-control" name='framework' value='{{ $user->profile->framework }}'>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Level</label>
                                        <input type="text" class="form-control" name='level' value='{{ $user->profile->level}}'>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Status Get Project</label>
                                        <select class="form-control" name="project_status">
                                            <option value="Available">Available</option>
                                            <option value="Not_avilable">Unavailable</option>
                                        </select>

                                    </div>

                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                            <!-- /.card-header -->

                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->

                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
    </section>


</div>
</section>
</div>

<!-- /.content -->
@endsection