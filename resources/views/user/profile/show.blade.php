@extends('user.layouts.app')

@section('content')
<div class="container">
    <section class="content-header">
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">

                        <!-- Profile Image -->
                        <div class="card card-primary card-outline">
                            <div class="card-body box-profile">
                                <div class="text-center">
                                    <img class="profile-user-img img-fluid img-circle" src="../../dist/img/user4-128x128.jpg" alt="User profile picture">
                                </div>
                                <!-- '', 'salary', 'workplace', 'activity_status','file_photo', 'file_cv', 'phone_number', 'address',  framework, level -->
                                <h3 class="profile-username text-center">{{ $user->profile->full_name }}</h3>

                                <p class="text-muted text-center">Programmer : {{ $user->profile->language_program }} | Status : {{ $user->profile->project_status }} </p>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->

                        <!-- About Me Box -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">About Me</h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <hr>

                                <strong><i class="far fa-file-alt mr-1"></i> Notes</strong>

                                <p class="text-muted">{{ $user->email }}</p>
                                <strong><i class="fas fa-phone mr-1"></i> Phone</strong>

                                <p class="text-muted">
                                    {{$user->profile->phone_number}}
                                </p>

                                <hr>

                                <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>

                                <p class="text-muted">{{$user->profile->address}}</p>
                               
                                <hr>

                                <strong><i class="fas fa-map-marker-alt mr-1"></i> Location</strong>

                                <p class="text-muted">{{$user->profile->address}}</p>

                                <hr>
                                <strong><i class="fas fa-map-marker-alt mr-1"></i> Job </strong>

                                <p class="text-muted">{{ $user->profile->activity_status}}, {{ $user->profile->workplace}}</p>

                                <hr>
                                <strong><i class="fas fa-pencil-alt mr-1"></i> Salary </strong>

                                <p class="text-muted"> Rp. {{ $user->profile->salary}}</p>

                                <hr>
                                <strong><i class="fas fa-pencil-alt mr-1"></i>Skills</strong>

                                <p class="text-muted">
                                    <span class="tag tag-danger">Program Languages : {{$user->profile->language_program}}</span> <br>
                                    <span class="tag tag-danger">Framework : {{$user->profile->framework}}</span><br>
                                    <span class="tag tag-danger">Framework : {{$user->profile->level}}</span>

                                </p>

                                <hr>

                                <strong><i class="far fa-file-alt mr-1"></i> Notes</strong>

                                <p class="text-muted">{{ $user->profile->email}}</p>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->

                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
    </section>


</div>
</section>
</div>

<!-- /.content -->
@endsection