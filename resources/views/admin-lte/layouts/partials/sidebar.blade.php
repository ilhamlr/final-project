<aside class="main-sidebar sidebar-light-primary elevation-4">
  <!-- Brand Logo -->
  <a href="../../index3.html" class="brand-link">
    <img src="{{ asset('/admin-lte/dist/img/AdminLTELogo.png')}}"
          alt="AdminLTE Logo"
          class="brand-image img-circle elevation-3"
          style="opacity: .8">
    <span class="brand-text font-weight-light">AdminLTE 3</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{ asset('/admin-lte/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">Alexander Pierce</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
              with font-awesome or any other icon font library -->
        <li class="nav-header">CRUD Resources</li>

        {{-- jika user adalah admin tampilkan menu di bawah ini --}}
        @if(\Auth::user()->role == 1)
        <li class="nav-item ">
          <a href="/admin/categories" class="nav-link">
            <i class="nav-icon fas fa-circle"></i>
            <p>
              Categories
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="/admin/products" class="nav-link">
            <i class="fas fa-circle nav-icon"></i>
            <p>Products</p>
          </a>
        </li>

        <li class="nav-item">
          <a href="/admin/products" class="nav-link">
            <i class="fas fa-circle nav-icon"></i>
            <p>Skill</p>
          </a>
        </li>

        <li class="nav-item">
            <a href="/admin/logout" class="nav-link">
              <i class="fas fa-circle nav-icon"></i>
              <p>Logout</p>
            </a>
        </li>
        
        @endif
        {{-- jika user adalah alumni tampilkan menu di bawah ini --}}
        @if(Auth::user()->role == 0)
        {{-- //menu untuk user --}}
        <li class="nav-item">
            <a href="/user/logout" class="nav-link">
              <i class="fas fa-circle nav-icon"></i>
              <p>Logout</p>
            </a>
        </li>
        @endif

      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>