@extends('admin-lte.layouts.app')


@section('content')
    <div class="container mx-auto bg-light mt-4">
        <form action="#" method="POST">

            <div class="form-group">
                <label for="name">Full Name:</label>
                <input type="text" class="form-control" id="name" name="name">
            </div>

            <div class="form-group">
                <label for="phone">Phone Number:</label>
                <input type="number" class="form-control" id="phone" name="name" required>
            </div>

            <div class="form-group">
                    <label for="salary">Salary Range:</label>
                    <select class="form-control" id="salary" name="name">
                      <option>1000000-2000000</option>
                      <option>2000000-3000000</option>
                      <option>3000000-4000000</option>
                      <option>4000000-5000000</option>
                    </select>
            </div>

            <div class="form-group">
                <label for="activity">Activity Status:</label>
                <input type="text" class="form-control" name="activity" id="activity" placeholder="Enter Activity Status">
            </div>

            <div class="form-group">
                <label for="project">Project Status:</label>
                <select class="form-control" id="project" name="project">
                <option>Available</option>
                <option>Not Available</option>
                </select>
            </div>

            <div class="form-group">
                <label for="photo">Photo:</label>
                <input type="file" class="form-control-file border" name="photo">
            </div>

            <div class="form-group">
                <label for="cv">Cv:</label>
                <input type="file" class="form-control-file border" name="cv">
            </div>

            <div class="form-group">
                <label for="address">address:</label>
                <textarea class="form-control" rows="5" id="address" name="address"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>    
@endsection