@extends('admin-lte.layouts.app')



@section('content')
 <div class="col-md-8">
  <form action="{{route('skills.store')}}" class="bg-white shadow-sm p-3 mx-auto " method="POST">
    {{csrf_field()}}

    <div class="form-group">
      <label for="language">Programming Language:</label>
      <input type="text" class="form-control" id="language" name="language">
    </div>

    <div class="form-group">
      <label for="framework">Framework:</label>
      <input type="text" class="form-control" id="framework" name="framework">
    </div>
    
    <div class="form-group">
        <label for="Level">Level:</label>
        <select class="form-control" id="level" name="level">
            <option>Beginner</option>
            <option>Competent</option>
            <option>Expert</option>
        </select>
    </div>

    <button type="submit" class="btn btn-primary">Save->(Skills)</button>
  </form>
</div>
@endsection